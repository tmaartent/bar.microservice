package com.thanksys.bar.microservice;

import com.thanksys.bar.microservice.dto.BeerDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class BreweryClient {
    @Value("${brewery.host}")
    private String host;

    public ResponseEntity<BeerDTO[]> getAllBeersOfACountry(String country) {
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate
                .getForEntity(host + "/beers/" + country, BeerDTO[].class);

    }
}
