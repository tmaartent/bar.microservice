package com.thanksys.bar.microservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thanksys.bar.microservice.dto.BeerDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles({"test"})
@AutoConfigureStubRunner(stubsMode = StubsMode.LOCAL, ids = {"com.thanksys:brewery.microservice:0.0.1:stubs:7070"})
public class BreweryClientTest  {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private BreweryClient breweryClient;

    @Test
    public void shouldBeAbleToRetrieveAListOfAllBelgianBeers() {
        ResponseEntity<BeerDTO[]> response = breweryClient.getAllBeersOfACountry("Belgium");

        assertThat(response.getStatusCode()).isEqualTo(OK);
        assertThat(response.getBody()).isEqualTo(new BeerDTO[]{
                leffe(),
                duvel(),
                stella()
        });
    }

    @Test
    public void shouldBeAbleToHandle404WhenRetrievingAListOfAllBeersForNonExistingCountry() throws Exception {
        try {
            breweryClient.getAllBeersOfACountry("Narnia");
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode()).isEqualTo(NOT_FOUND);
        }
    }

    private BeerDTO stella() {
        return BeerDTO.builder()
                .name("stella")
                .alcoholPercentage(5)
                .build();
    }

    private BeerDTO duvel() {
        return BeerDTO.builder()
                .name("duvel")
                .alcoholPercentage(8)
                .build();
    }

    private BeerDTO leffe() {
        return BeerDTO.builder()
                .name("leffe")
                .alcoholPercentage(6)
                .build();
    }
}